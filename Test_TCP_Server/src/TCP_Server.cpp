#include <Arduino.h>
#include <ESP8266WiFi.h>

#define SERVER_PORT 9090
WiFiServer server(SERVER_PORT);
WiFiClient client;

void setup()
{
  Serial.begin(9600);
  delay(1000);
  WiFi.begin("Maaad", "12345678");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());

  //server.begin(SERVER_PORT);
}

void loop()
{
    if (!client.connected()) {
        // try to connect to a new client
        client = server.available();
    } else {
        // read data from the connected client
        if (client.available() > 0) {
            Serial.write(client.read());
        }
    }   
}