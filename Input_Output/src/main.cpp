#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
WiFiUDP Udp;
char in_buffer[255];
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(D0, INPUT);
  Serial.begin(9600);
  delay(1000);
  WiFi.begin("Maaad", "12345678");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());
  Udp.begin(8090);
}
void loop()
{
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    int len = Udp.read(in_buffer, 255);
    Serial.println(in_buffer);
  }
}
#if 0
void loop()
{
  static int i = 0;
  //int x = digitalRead(D0);
  digitalWrite(LED_BUILTIN, HIGH);

  delay(500);
  if (Serial.available() > 0)
  {
    delay(10);
    String s = Serial.readString();

    Serial.printf("Salam ");

    Serial.write(s.c_str());
  }

  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
}
#endif