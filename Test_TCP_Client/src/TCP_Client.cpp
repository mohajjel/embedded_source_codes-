#include <Arduino.h>
#include <ESP8266WiFi.h>

#define SERVER_IP "192.168.43.1"
#define SERVER_PORT 8080

void setup()
{
  Serial.begin(9600);
  delay(1000);
  WiFi.begin("Maaad", "12345678");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());
}

void loop()
{
  WiFiClient client;
  if (!client.connect(SERVER_IP, SERVER_PORT))
  {
    Serial.println("connection failed");
    delay(2000);
    return;
  }

  unsigned long timeout = millis();
  int len=0;
  while ((len=client.available()) == 0)
  {
    if (millis() - timeout > 5000)
    {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  char * buffer = new char[len+1];

  client.readBytes(buffer, len);
  buffer[len]=0;
  Serial.println(buffer);
  delete [] buffer;
  client.stop();
  
}