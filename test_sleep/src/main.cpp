#include <Arduino.h>

/**
 * An example showing how to put ESP8266 into Deep-sleep mode
 */
int x=1;
void setup()
{

  Serial.begin(9600);
  Serial1.begin(9600);
  Serial.setTimeout(2000);

  // Wait for serial to initialize.
  while (!Serial)
  {
  }

  //Serial.println("I'm awake.");
  //Serial.printf("%d\r\n",analogRead(A0));
  //Serial.println("Going into deep sleep for 20 seconds");
  
  //ESP.deepSleep(20e6); // 20e6 is 20 microseconds
  
}

void loop()
{
  Serial.printf("%d\r\n",analogRead(A0));
  Serial1.printf("%d\r\n",analogRead(A0));
  
  delay(1000);

}