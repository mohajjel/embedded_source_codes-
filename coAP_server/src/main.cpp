#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <coap.h>

#define SSID "Maad2"
#define SSID_PASSWOD "m1234567"

// CoAP client response callback
void callback_response(CoapPacket &packet, IPAddress ip, int port);

// CoAP server endpoint url callback
void callback_light(CoapPacket &packet, IPAddress ip, int port);

void callback_debug(CoapPacket &packet, IPAddress ip, int port);

// UDP and CoAP class
WiFiUDP Udp;
Coap coap(Udp);

// LED STATE
bool LEDSTATE;

// CoAP server endpoint URL
void callback_light(CoapPacket &packet, IPAddress ip, int port)
{
  Serial.print("[Light]");
  Serial.printf(" From %s:%d\r\n",ip.toString().c_str(),port);

  // send response
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;

  String message(p);

  if (message.equals("0"))
    LEDSTATE = false;
  else if (message.equals("1"))
    LEDSTATE = true;

  if (LEDSTATE)
  {
    digitalWrite(LED_BUILTIN, LOW);
    //if(packet.type == COAP_CON)
    coap.sendResponse(ip, port, packet.messageid, "1");
    //coap.send(ip,port,"", COAP_ACK, COAP_GET, packet.token, packet.tokenlen, NULL,0);
   // coap.sendResponse(ip, port, packet.messageid, "1", 1, COAP_CONTENT, COAP_TEXT_PLAIN,packet.token, packet.tokenlen);
  }
  else
  {
    digitalWrite(LED_BUILTIN, HIGH);
    coap.sendResponse(ip, port, packet.messageid, "0");
    //coap.sendResponse(ip, port, packet.messageid, "0", 1, COAP_CONTENT, COAP_TEXT_PLAIN,packet.token, packet.tokenlen);
  }
}

//callback debug
void callback_debug(CoapPacket &packet, IPAddress ip, int port)
{
  Serial.println("Debug mode");
  if (packet.code == COAP_POST)
  {
    Serial.println("POST method");
  }
  //print token
  if (packet.tokenlen > 0)
  {
    Serial.printf("0x");
    for (uint8_t i = 0; i < packet.tokenlen; i++)
    {
      Serial.printf("%x", packet.token[i]);
    }
  }
  Serial.println("");
  for (int i = 0; i < packet.optionnum; i++)
  {
    if (packet.options[i].number == COAP_OBSERVE)
    {
      char str[packet.options[i].length + 1];
      memcpy(str, packet.options[i].buffer, packet.options[i].length);
      str[packet.options[i].length] = NULL;
      Serial.printf("%s\r\n", str);
    }
  }

  // send response
  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = NULL;

  String message(p);

  if (message.equals("0"))
    LEDSTATE = false;
  else if (message.equals("1"))
    LEDSTATE = true;

  if (LEDSTATE)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    coap.sendResponse(ip, port, packet.messageid, "1");
  }
  else
  {
    digitalWrite(LED_BUILTIN, LOW);
    coap.sendResponse(ip, port, packet.messageid, "0");
  }
}

// CoAP client response callback
void callback_response(CoapPacket &packet, IPAddress ip, int port)
{
  Serial.println("[Coap Response got]");

  char p[packet.payloadlen + 1];
  memcpy(p, packet.payload, packet.payloadlen);
  p[packet.payloadlen] = 0;

  Serial.println(p);
}

void setup()
{
  // Start Serial
  Serial.begin(9600);

  // Connect to WiFi
  WiFi.begin(SSID, SSID_PASSWOD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());

  // LED State
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);
  LEDSTATE = true;

  // add server url endpoints.
  // can add multiple endpoint urls.
  // exp) coap.server(callback_switch, "switch");
  //      coap.server(callback_env, "env/temp");
  //      coap.server(callback_env, "env/humidity");
  Serial.println("Setup Callback Light");
  coap.server(callback_light, "light");
  coap.server(callback_debug, "debug");

  // client response callback.
  // this endpoint is single callback.
  Serial.println("Setup Response Callback");
  coap.response(callback_response);

  // start coap server/client
  coap.start();
}

void loop()
{
  // send GET or PUT coap request to CoAP server.
  // To test, use libcoap, microcoap server...etc
  // int msgid = coap.put(IPAddress(10, 0, 0, 1), 5683, "light", "1");
  //Serial.println("Send Request");
  // int msgid = coap.get(IPAddress(XXX, XXX, XXX, XXX), 5683, "time");

  delay(1000);
  coap.loop();
  //if (f == 1)
    //coap.sendResponse();
}
/*
if you change LED, req/res test with coap-client(libcoap), run following.
coap-client -m get coap://(arduino ip addr)/light
coap-client -e "1" -m put coap://(arduino ip addr)/light
coap-client -e "0" -m put coap://(arduino ip addr)/light
*/
